import os

from llama_index import ServiceContext
from llama_index.llms.vertex import Vertex
from app.engine.constants import GOOGLE_PROJECT_ID
from langchain_google_vertexai import VertexAIEmbeddings
import google.oauth2.credentials
from typing import Optional

def create_base_context(access_token: Optional[str] = None):
    model = os.getenv("MODEL", "gemini-pro")    
    credentials = google.oauth2.credentials.Credentials(token=access_token)
    embeddingModel = VertexAIEmbeddings(
        project=GOOGLE_PROJECT_ID,
        model_name="textembedding-gecko@001",
        credentials=credentials
        )

    return ServiceContext.from_defaults(
        llm = Vertex(
            model=model,
            project=GOOGLE_PROJECT_ID,
            credentials=credentials,
            max_tokens = 8192,
        ),
        embed_model=embeddingModel
    )

