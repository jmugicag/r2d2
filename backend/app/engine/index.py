from llama_index.chat_engine import SimpleChatEngine
from typing import Optional

from app.context import create_base_context


def get_chat_engine(access_token: Optional[str] = None):
    service_context=create_base_context(access_token)
    return SimpleChatEngine.from_defaults(service_context)
