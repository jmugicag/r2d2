from typing import List
from fastapi.responses import StreamingResponse
from llama_index.chat_engine.types import BaseChatEngine

from app.engine.index import get_chat_engine
from app.engine.jwe import get_token
from fastapi import APIRouter, Depends, HTTPException, Request, status
from llama_index.llms.base import ChatMessage
from llama_index.llms.types import MessageRole
from pydantic import BaseModel

chat_router = r = APIRouter()


class _Message(BaseModel):
    role: MessageRole
    content: str


class _ChatData(BaseModel):
    messages: List[_Message]


def get_chat_engine_with_token(request: Request):
    cookie_name = 'authjs.session-token'
    session_token = request.cookies.get(cookie_name)
    print("session_token: ",session_token)
    if (session_token == None):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="No access token was provided. Please authenticate first.",
        )
    decoded_session_token = get_token(session_token,cookie_name)
    print("decoded_session_token: ",decoded_session_token)
    chat_engine = get_chat_engine(decoded_session_token['accessToken'])
    return chat_engine

@r.post("")
async def chat(
    request: Request,
    data: _ChatData,
    chat_engine: BaseChatEngine = Depends(get_chat_engine_with_token),
):
    # check preconditions and get last message
    if len(data.messages) == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="No messages provided",
        )
    lastMessage = data.messages.pop()
    if lastMessage.role != MessageRole.USER:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Last message must be from user",
        )
    # convert messages coming from the request to type ChatMessage
    messages = [
        ChatMessage(
            role=m.role,
            content=m.content,
        )
        for m in data.messages
    ]

    # response = chat_engine.astream_chat(lastMessage.content, messages)
    response = chat_engine.stream_chat(lastMessage.content, messages)

    # print("response: ", response)

    # # stream response
    # async def event_generator():
    #     async for token in response.async_response_gen():
    #         # If client closes connection, stop sending events
    #         if await request.is_disconnected():
    #             break
    #         yield token

    return StreamingResponse(response.response_gen, media_type="text/plain")
