import type { Metadata } from "next";
import { Inter, Roboto_Mono } from "next/font/google";
import { SessionProvider } from "next-auth/react";
import { auth } from "./api/auth";
import HeadBar from "./components/ui/header-components/HeadBar";
import LoginGoogle from "./components/ui/header-components/Login";
import "./globals.css";
import SignInMessage from './components/ui/utils-components/SignInMessage';

const inter = Inter({ subsets: ['latin'] })

// export const roboto_mono = Roboto_Mono({
//   subsets: ['latin'],
//   display: 'swap',
//   weight: "400"
// })

export const metadata: Metadata = {
  title: 'Playground',
  description: 'Designed for CE',
}

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await auth();
  return (
    <html lang="en">
      <body className={inter.className}>
            <SessionProvider session={session}>
                <HeadBar>
                    <LoginGoogle />
                </HeadBar>
                {session &&
                    <> {children} </>
                }
                {!session &&
                    <SignInMessage callbackUrl={"/"}/>
                }
            </SessionProvider>
      </body>
    </html>
  );
}
